import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Schueler implements Serializable {
    private String name;
    private String vorname;
    private transient int kennung;
    private transient long sozialversicherungsnummer;


    public Schueler(String name, String vorname, int kennung, long sozialversicherungsnummer) {
        this.name = name;
        this.vorname = vorname;
        if (checkKennung(kennung)){
            this.kennung = kennung;
            if (checkSVN(sozialversicherungsnummer)){
                this.sozialversicherungsnummer = sozialversicherungsnummer;
            }else{
                throw new IllegalArgumentException("Ungueltige SVN: " + sozialversicherungsnummer);
            }
        } else {
            throw new IllegalArgumentException("Ungueltige Kennung: " + kennung);
        }
    }



    public static String encrypt(String decryptedString, int verrueckung) {
        decryptedString = decryptedString.toUpperCase();
        String erg = "";
        for (int i = 0; i < decryptedString.length(); i++) {
            char ch = decryptedString.charAt(i);
            if (!Character.isLetter(ch)) {
                erg += ch;
            }
            else {
                ch = (char) (((int) ch + verrueckung - 65) % 26 + 65);
                erg += ch;
            }
        }
        return erg;
    }

    public static String decrypt(String encryptetString, int verrueckung) {
        String erg = "";
        for (int i = 0; i < encryptetString.length(); i++) {
            char ch = encryptetString.charAt(i);
            if (!Character.isLetter(ch)) {
                erg += ch;
            }
            else {
                ch = (char) (((int) ch - verrueckung - 65 + 26) % 26 + 65);
                erg += ch;
            }
        }
        return erg;
    }

    public static boolean checkSVN(long sozialversicherungsnummer){
        String[] svn = (sozialversicherungsnummer+ "").split("");
        if (svn.length!=10){
            return false;
        }else{
            if ((Integer.parseInt(svn[0]) * 3 + Integer.parseInt(svn[1]) * 7 + Integer.parseInt(svn[2]) * 9 + Integer.parseInt(svn[4]) * 5+
                    Integer.parseInt(svn[5]) * 8 + Integer.parseInt(svn[6]) * 4 + Integer.parseInt(svn[7]) * 2 + Integer.parseInt(svn[8])+
                    Integer.parseInt(svn[9]) * 6)%11 == Integer.parseInt(svn[3])){
                return true;
            }
            return false;
        }
    }

    public static boolean checkKennung(int kennung){
        String k = kennung + "";
        if (k.length()!=8){
            return false;
        }
        int jahr = Integer.parseInt(k.substring(0, 4));
        if (jahr> LocalDate.now().getYear()){
            return false;
        }
        return true;
    }

    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeUTF(encrypt(getZahlInWord(this.sozialversicherungsnummer), -4));
        objectOutputStream.writeUTF(encrypt(getZahlInWord(this.kennung), -4));
    }

    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        this.setSozialversicherungsnummer(getWordInZahl(decrypt(objectInputStream.readUTF(), -4)));
        this.setKennung(getWordInZahl(decrypt(objectInputStream.readUTF(), -4)));
    }

    public static String getZahlInWord(long zahl){
        String strSvn = zahl + "";
        String[] zahlen = {"null", "eins", "zwei", "drei", "vier", "fuenf", "sechs","sieben", "acht", "neun"};
        StringBuilder erg = new StringBuilder();
        for (int i = 0; i<strSvn.length(); i++) {
            erg.append(zahlen[Integer.parseInt(String.valueOf(strSvn.charAt(i)))]).append(" ");
        }
        return erg.toString();
    }

    public static int getWordInZahl(String word){
        String[] numbers = word.split(" ");
        StringBuilder erg = new StringBuilder();
        String[] zahlen = {"null", "eins", "zwei", "drei", "vier", "fuenf", "sechs","sieben", "acht", "neun"};
        for (String s:numbers) {
            switch (s) {
                case "null" -> erg.append("0");
                case "eins" -> erg.append("1");
                case "zwei" -> erg.append("2");
                case "drei" -> erg.append("3");
                case "vier" -> erg.append("4");
                case "fuenf" -> erg.append("5");
                case "sechs" -> erg.append("6");
                case "sieben" -> erg.append("7");
                case "acht" -> erg.append("8");
                case "neun" -> erg.append("9");
            }
        }
        return Integer.parseInt(erg.toString());
    }

    public static void main(String[] args) {
        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream)) {

            List<Schueler> schuelerList = new ArrayList<>(List.of(new Schueler("Pinz", "Adrian", 20200567, 3847150606L),
                                                                            new Schueler("Maier", "Anna", 20200567, 3847150606L),
                                                                new Schueler("Schmidt", "Max", 20200567, 3847150606L),
                                                                new Schueler("Müller", "Lena", 20200567, 3847150606L),
                                                                                new Schueler("Wagner", "Tom", 20200567, 3847150606L),
                                                            new Schueler("Klein", "Lisa", 20200567, 3847150606L)));

            for (Schueler schueler:schuelerList) {
                System.out.println(schueler);
                schueler.writeObject(objectOutputStream);
                System.out.println(schueler);
            }
            byte[] bytes1 = byteArrayOutputStream.toByteArray();
            System.out.println(Arrays.toString(bytes1));

        } catch (IOException e) {
            System.out.println(e.getLocalizedMessage());
        }
    }

    public void setKennung(int kennung) {
        this.kennung = kennung;
    }

    public void setSozialversicherungsnummer(long sozialversicherungsnummer) {
        this.sozialversicherungsnummer = sozialversicherungsnummer;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.name).append(this.vorname).append(this.sozialversicherungsnummer).append(this.kennung);
        return stringBuilder.toString();
    }
}
